const express = require("express");
const serverless = require('serverless-http');
const app = express();

const replaceStream = require('replacestream');
const request = require('request');
const port = 5000;

const PODCAST_URL = "https://www.ivoox.com/aguacates-mutantes_fg_f1882567_filtro_1.xml";
const PODCAST_NAME = "Aguacates Mutantes";
const PODCAST_EMAIL = "aguacates.mutantes@gmail.com";

const router = express.Router();


// Home route
router.get("/", (req, res) => {
  res.status(200);
  request.get(PODCAST_URL)
    .pipe(replaceStream(`<itunes:author><![CDATA[${PODCAST_NAME}]]></itunes:author>`,
   `<itunes:author><![CDATA[${PODCAST_NAME}]]></itunes:author><itunes:owner><itunes:email>${PODCAST_EMAIL}</itunes:email></itunes:owner>`))
    .pipe(res);
});

//app.use(express.urlencoded({ extended: false }));
app.use('/.netlify/functions/rss', router);

module.exports.handler = serverless(app);
